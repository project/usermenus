<?php

/**
 * @file
 * Administrative page callbaks for usermenus module.
 */

/**
 * Menu callback which shows an overview page of all the custom menus and their descriptions.
 */
function usermenus_overview_page() {
  $result = db_query("SELECT * FROM {menu_users} ORDER BY title");
  $content = array();
  while ($menu = db_fetch_array($result)) {
    $menu['href'] = 'admin/build/usermenus-customize/'. $menu['menu_name'];
    $menu['localized_options'] = array();
    $content[] = $menu;
  }
  return theme('admin_block_content', $content);
}

/**
 * Form for editing an entire menu tree at once.
 *
 * Shows for one menu the menu items accessible to the current user and
 * relevant operations.
 */
function usermenus_overview_form(&$form_state, $menu) {
  global $_menu_admin;
  global $user;
  $sql = "
    SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
    FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
    WHERE ml.menu_name = '%s'
    ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";
  if ($menu['menu_name']=='enumresuym') {
    $menu['menu_name']='usermenu-'. $user->name;
  }
  $result = db_query($sql, $menu['menu_name']);
  $tree = menu_tree_data($result);
  $node_links = array();
  menu_tree_collect_node_links($tree, $node_links);
  // We indicate that a menu administrator is running the menu access check.
  $_menu_admin = TRUE;
  menu_tree_check_access($tree, $node_links);
  $_menu_admin = FALSE;

  $form = _usermenus_overview_tree_form($tree, $menu);
  $form['#menu'] =  $menu;
  if (element_children($form)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }
  else {
    $form['empty_menu'] = array('#value' => t('There are no menu items yet.'));
  }
  return $form;
}

/**
 * Recursive helper function for menu_overview_form().
 */
function _usermenus_overview_tree_form($tree, $menu) {
  global $user;
  static $form = array('#tree' => TRUE);
  $actionpaths = 'admin/build/usermenus-customize/';
  if (user_access('administer own usermenus') && $menu['menu_name']=='usermenu-'. $user->name) {
    $actionpaths = 'usermenus/';
  }
  foreach ($tree as $data) {
    $title = '';
    $item = $data['link'];
    // Don't show callbacks; these have $item['hidden'] < 0.
    if ($item && $item['hidden'] >= 0) {
      $mlid = 'mlid:'. $item['mlid'];
      $form[$mlid]['#item'] = $item;
      $form[$mlid]['#attributes'] = $item['hidden'] ? array('class' => 'menu-disabled') : array('class' => 'menu-enabled');
      if ($item['href']=='folder') {
        $form[$mlid]['title']['#value'] = $item['title'] . ($item['hidden'] ? ' ('. t('disabled') .')' : '');
      }
      else {
        $form[$mlid]['title']['#value'] = l($item['title'], $item['href'], $item['localized_options']) . ($item['hidden'] ? ' ('. t('disabled') .')' : '');
      }
      $form[$mlid]['hidden'] = array(
        '#type' => 'checkbox',
        '#default_value' => !$item['hidden'],
      );
      $form[$mlid]['expanded'] = array(
        '#type' => 'checkbox',
        '#default_value' => $item['expanded'],
      );
      $form[$mlid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => isset($form_state[$mlid]['weight']) ? $form_state[$mlid]['weight'] : $item['weight'],
      );
      $form[$mlid]['mlid'] = array(
        '#type' => 'hidden',
        '#value' => $item['mlid'],
      );
      $form[$mlid]['plid'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($form_state[$mlid]['plid']) ? $form_state[$mlid]['plid'] : $item['plid'],
        '#size' => 6,
      );
      // Build a list of operations.
      $operations = array();
      $operations['edit'] = l(t('edit'), $actionpaths .'item/edit/'. $item['mlid']);
      // Only items created by the usermenus module can be deleted.
      if ($item['module'] == 'usermenus' || $item['updated'] == 1) {
        $operations['delete'] = l(t('delete'), $actionpaths .'item/delete/'. $item['mlid']);
      }

      $form[$mlid]['operations'] = array();
      foreach ($operations as $op => $value) {
        $form[$mlid]['operations'][$op] = array('#value' => $value);
      }
    }

    if ($data['below']) {
      _usermenus_overview_tree_form($data['below'], $menu);
    }
  }
  return $form;
}

/**
 * Submit handler for the menu overview form.
 *
 * This function takes great care in saving parent items first, then items
 * underneath them. Saving items in the incorrect order can break the menu tree.
 *
 * @see usermenus_overview_form()
 */
function usermenus_overview_form_submit($form, &$form_state) {
  // When dealing with saving menu items, the order in which these items are
  // saved is critical. If a changed child item is saved before its parent,
  // the child item could be saved with an invalid path past its immediate
  // parent. To prevent this, save items in the form in the same order they
  // are sent by $_POST, ensuring parents are saved first, then their children.
  // See http://drupal.org/node/181126#comment-632270
  $order = array_flip(array_keys($form['#post'])); // Get the $_POST order.
  $form = array_merge($order, $form); // Update our original form with the new order.

  $updated_items = array();
  $fields = array('expanded', 'weight', 'plid');
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $element = $form[$mlid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field) {
        if ($element[$field]['#value'] != $element[$field]['#default_value']) {
          $element['#item'][$field] = $element[$field]['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
      // Hidden is a special case, the value needs to be reversed.
      if ($element['hidden']['#value'] != $element['hidden']['#default_value']) {
        $element['#item']['hidden'] = !$element['hidden']['#value'];
        $updated_items[$mlid] = $element['#item'];
      }
    }
  }

  // Save all our changed items to the database.
  foreach ($updated_items as $item) {
    $item['customized'] = 1;
    menu_link_save($item);
  }
}

/**
 * Theme the menu overview form into a table.
 *
 * @ingroup themeable
 */
function theme_usermenus_overview_form($form) {
  drupal_add_tabledrag('usermenus-overview', 'match', 'parent', 'menu-plid', 'menu-plid', 'menu-mlid', TRUE, MENU_MAX_DEPTH - 1);
  drupal_add_tabledrag('usermenus-overview', 'order', 'sibling', 'menu-weight');

  $header = array(
    t('Menu item'),
    array('data' => t('Enabled'), 'class' => 'checkbox'),
    array('data' => t('Expanded'), 'class' => 'checkbox'),
    t('Weight'),
    array('data' => t('Operations'), 'colspan' => '3'),
  );

  $rows = array();
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['hidden'])) {
      $element = &$form[$mlid];
      // Build a list of operations.
      $operations = array();
      foreach (element_children($element['operations']) as $op) {
        $operations[] = drupal_render($element['operations'][$op]);
      }
      while (count($operations) < 2) {
        $operations[] = '';
      }

      // Add special classes to be used for tabledrag.js.
      $element['plid']['#attributes']['class'] = 'menu-plid';
      $element['mlid']['#attributes']['class'] = 'menu-mlid';
      $element['weight']['#attributes']['class'] = 'menu-weight';

      // Change the parent field to a hidden. This allows any value but hides the field.
      $element['plid']['#type'] = 'hidden';

      $row = array();
      $row[] = theme('indentation', $element['#item']['depth'] - 1) . drupal_render($element['title']);
      $row[] = array('data' => drupal_render($element['hidden']), 'class' => 'checkbox');
      $row[] = array('data' => drupal_render($element['expanded']), 'class' => 'checkbox');
      $row[] = drupal_render($element['weight']) . drupal_render($element['plid']) . drupal_render($element['mlid']);
      $row = array_merge($row, $operations);

      $row = array_merge(array('data' => $row), $element['#attributes']);
      $row['class'] = !empty($row['class']) ? $row['class'] .' draggable' : 'draggable';
      $rows[] = $row;
    }
  }
  $output = '';
  if ($rows) {
    $output .= theme('table', $header, $rows, array('id' => 'usermenus-overview'));
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Menu callback; Build the menu link editing form.
 */
function usermenus_edit_item(&$form_state, $type, $item, $menu) {
  global $user;

  $form['usermenus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu settings'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#weight' => -2,
    '#attributes' => array('class' => 'menu-item-form'),
    '#item' => $item,
  );
  if ($menu['menu_name']=='enumresuym') {
    $menu['menu_name']='usermenu-'. $user->name;
  }
  if ($type == 'add' || empty($item)) {
    // This is an add form, initialize the menu link.
    $item = array('link_title' => '', 'mlid' => 0, 'plid' => 0, 'menu_name' => $menu['menu_name'], 'weight' => 0, 'link_path' => '', 'options' => array(), 'module' => 'usermenus', 'expanded' => 0, 'hidden' => 0, 'has_children' => 0, 'newwindow' => 0);
    if ($_GET['q']!='usermenus/mine/add' && $_GET['q']!='usermenus/mine/edit') {
      $item['link_path']=str_replace('usermenus/mine/addpage/', '', $_GET['q']);
      $menu_item = menu_get_item($item['link_path']);
      $item['link_title'] = $menu_item['title'];
    }
  }
  if ($type == 'addpage') {
    // This is an add this page form, initialize the menu link.
    $item = array('link_title' => '', 'mlid' => 0, 'plid' => 0, 'menu_name' => $menu['menu_name'], 'weight' => 0, 'link_path' => '', 'options' => array(), 'module' => 'usermenus', 'expanded' => 0, 'hidden' => 0, 'has_children' => 0, 'newwindow' => 0);
    if ($_GET['q']!='usermenus/mine/add' && $_GET['q']!='usermenus/mine/edit') {
      $item['link_path']=drupal_get_normal_path(drupal_get_path_alias(str_replace('usermenus/item/addpage/', '', $_GET['q'])));
      $menu_item = menu_get_item($item['link_path']);
      $item['link_title'] = $menu_item['title'];
    }
  }
  foreach (array('link_path', 'mlid', 'module', 'has_children', 'options') as $key) {
    $form['usermenus'][$key] = array('#type' => 'value', '#value' => $item[$key]);
  }
  // Any item created or edited via this interface is considered "customized".
  $form['usermenus']['customized'] = array('#type' => 'value', '#value' => 1);
  $form['usermenus']['original_item'] = array('#type' => 'value', '#value' => $item);

  $path = $item['link_path'];
  if (isset($item['options']['query'])) {
    $path .= '?'. $item['options']['query'];
  }
  if (isset($item['options']['fragment'])) {
    $path .= '#'. $item['options']['fragment'];
  }
  if ($item['module'] == 'usermenus') {
    $form['usermenus']['link_path'] = array(
      '#type' => (($_GET['q']!='usermenus/mine/add' && $_GET['q']!='usermenus/mine/edit') ? 'hidden' : 'textfield'),
      '#title' => t('Path'),
      '#default_value' => $path,
      '#description' => t('The path this menu item links to. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page. Enter %folder to make this item a folder.', array('%front' => '<front>', '%add-node' => 'node/add', '%drupal' => 'http://drupal.org', '%folder' => '<folder>')),
      '#required' => TRUE,
    );
  }
  else {
    $form['usermenus']['_path'] = array(
      '#type' => 'item',
      '#title' => t('Path'),
      '#description' => l($item['link_title'], $item['href'], $item['options']),
    );
  }
  $form['usermenus']['link_title'] = array('#type' => 'textfield',
    '#title' => t('Menu link title'),
    '#default_value' => $item['link_title'],
    '#description' => t('The link text corresponding to this item that should appear in the menu.'),
    '#required' => TRUE,
  );
  $form['usermenus']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($item['options']['attributes']['title']) ? $item['options']['attributes']['title'] : '',
    '#rows' => 1,
    '#description' => t('The description displayed when hovering over a menu item.'),
  );
  $form['usermenus']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => !$item['hidden'],
    '#description' => t('Menu items that are not enabled will not be listed in any menu.'),
  );
  $form['usermenus']['expanded'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expanded'),
    '#default_value' => $item['expanded'],
    '#description' => t('If selected and this menu item has children, the menu will always appear expanded.'),
  );

  // Generate a list of possible parents (not including this item or descendants).
  $options = menu_parent_options(usermenus_get_menus(FALSE, $menu['menu_name']), $item);
  $default = $item['menu_name'] .':'. $item['plid'];
  $form['usermenus']['parent'] = array(
    '#type' => 'select',
    '#title' => t('Parent item'),
    '#default_value' => $default,
    '#options' => $options,
    '#description' => t('The maximum depth for an item and all its children is fixed at !maxdepth. Some menu items may not be available as parents if selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
    '#attributes' => array('class' => 'menu-title-select'),
  );
  $form['usermenus']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $item['weight'],
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Validate form values for a menu link being added or edited.
 */
function usermenus_edit_item_validate($form, &$form_state) {
  $item = &$form_state['values']['usermenus'];
  if ($item['link_path']=='<folder>') {
    $item['link_path']='folder';
  }
  else {
    $normal_path = drupal_get_normal_path($item['link_path']);
    if ($item['link_path'] != $normal_path) {
      drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $item['link_path'], '%normal_path' => $normal_path)));
      $item['link_path'] = $normal_path;
    }
    if (!menu_path_is_external($item['link_path'])) {
      $parsed_link = parse_url($item['link_path']);
      if (isset($parsed_link['query'])) {
        $item['options']['query'] = $parsed_link['query'];
      }
      if (isset($parsed_link['fragment'])) {
        $item['options']['fragment'] = $parsed_link['fragment'];
      }
      if ($item['link_path'] != $parsed_link['path']) {
        $item['link_path'] = $parsed_link['path'];
      }
    }
    if (!trim($item['link_path']) || !menu_valid_path($item)) {
      form_set_error('link_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $item['link_path'])));
    }
  }
}

/**
 * Process menu and menu item add/edit form submissions.
 */
function usermenus_edit_item_submit($form, &$form_state) {
  global $user;
  $item = $form_state['values']['usermenus'];

  // The value of "hidden" is the opposite of the value
  // supplied by the "enabled" checkbox.
  $item['hidden'] = (int) !$item['enabled'];
  unset($item['enabled']);

  $item['options']['attributes']['title'] = $item['description'];
  list($item['menu_name'], $item['plid']) = explode(':', $item['parent']);
  if (!menu_link_save($item)) {
    drupal_set_message(t('There was an error saving the menu link.'), 'error');
  }
  //If they are editing their menu, they should be doing so with the My Menu link.
  if (user_access('administer own usermenus') && $item['menu_name']=='usermenu-'. $user->name) {
    $form_state['redirect'] = 'usermenus/mine';
  }
  else {
    $form_state['redirect'] = 'admin/build/usermenus-customize/'. $item['menu_name'];
  }
}

/**
 * Menu callback; Build the form that handles the adding/editing of a custom menu.
 */
function usermenus_edit_menu(&$form_state, $type, $menu = array()) {
  if ($type == 'edit') {
    $form['menu_name'] = array('#type' => 'value', '#value' => $menu['menu_name']);
    $form['#insert'] = FALSE;
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#access' => !in_array($menu['menu_name'], menu_list_system_menus()),
      '#submit' => array('usermenus_users_delete_submit'),
      '#weight' => 10,
    );
  }
  else {
    $menu = array('menu_name' => '', 'title' => '', 'description' => '');
    $form['menu_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Menu name'),
      '#maxsize' => MENU_MAX_MENU_NAME_LENGTH_UI,
      '#description' => t('The machine-readable name of this menu. This text will be used for constructing the URL of the <em>menu overview</em> page for this menu. This name must contain only lowercase letters, numbers, and hyphens, and must be unique.'),
      '#required' => TRUE,
    );
    $form['#insert'] = TRUE;
  }
  $form['#title'] = $menu['title'];
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $menu['title'],
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $menu['description'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit function for the 'Delete' button on the menu editing form.
 */
function usermenus_users_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/build/usermenus-customize/'. $form_state['values']['menu_name'] .'/delete';
}

/**
 * Menu callback; check access and get a confirm form for deletion of a custom menu.
 */
function usermenus_delete_menu_page($menu) {
  // System-defined menus may not be deleted.
  if (in_array($menu['menu_name'], menu_list_system_menus())) {
    drupal_access_denied();
    return;
  }
  return drupal_get_form('usermenus_delete_menu_confirm', $menu);
}

/**
 * Build a confirm form for deletion of a custom menu.
 */
function usermenus_delete_menu_confirm(&$form_state, $menu) {
  $form['#menu'] = $menu;
  $caption = '';
  $num_links = db_result(db_query("SELECT COUNT(*) FROM {menu_links} WHERE menu_name = '%s'", $menu['menu_name']));
  if ($num_links) {
    $caption .= '<p>'. format_plural($num_links, '<strong>Warning:</strong> There is currently 1 menu item in %title. It will be deleted (system-defined items will be reset).', '<strong>Warning:</strong> There are currently @count menu items in %title. They will be deleted (system-defined items will be reset).', array('%title' => $menu['title'])) .'</p>';
  }
  $caption .= '<p>'. t('This action cannot be undone.') .'</p>';
  return confirm_form($form, t('Are you sure you want to delete the custom menu %title?', array('%title' => $menu['title'])), 'admin/build/usermenus-customize/'. $menu['menu_name'], $caption, t('Delete'));
}

/**
 * Delete a custom menu and all items in it.
 */
function usermenus_delete_menu_confirm_submit($form, &$form_state) {
  $menu = $form['#menu'];
  $form_state['redirect'] = 'admin/build/usermenus';
  // System-defined menus may not be deleted - only menus defined by this module.
  if (in_array($menu['menu_name'], menu_list_system_menus())  || !db_result(db_query("SELECT COUNT(*) FROM {menu_users} WHERE menu_name = '%s'", $menu['menu_name']))) {
    return;
  }
  // Reset all the menu links defined by the system via hook_menu.
  $result = db_query("SELECT * FROM {menu_links} ml INNER JOIN {menu_router} m ON ml.router_path = m.path WHERE ml.menu_name = '%s' AND ml.module = 'system' ORDER BY m.number_parts ASC", $menu['menu_name']);
  while ($item = db_fetch_array($result)) {
    menu_reset_item($item);
  }
  // Delete all links to the overview page for this menu.
  $result = db_query("SELECT mlid FROM {menu_links} ml WHERE ml.link_path = '%s'", 'admin/build/usermenus-customize/'. $menu['menu_name']);
  while ($m = db_fetch_array($result)) {
    menu_link_delete($m['mlid']);
  }
  // Delete all the links in the menu and the menu from the list of custom menus.
  db_query("DELETE FROM {menu_links} WHERE menu_name = '%s'", $menu['menu_name']);
  db_query("DELETE FROM {menu_users} WHERE menu_name = '%s'", $menu['menu_name']);
  // Delete all the blocks for this menu.
  db_query("DELETE FROM {blocks} WHERE module = 'usermenus' AND delta = '%s'", $menu['menu_name']);
  db_query("DELETE FROM {blocks_roles} WHERE module = 'usermenus' AND delta = '%s'", $menu['menu_name']);
  menu_cache_clear_all();
  cache_clear_all();
  $t_args = array('%title' => $menu['menu_name']);
  drupal_set_message(t('The user menu %title has been deleted.', $t_args));
  watchdog('menu', 'Deleted user menu %title and all its menu items.', $t_args, WATCHDOG_NOTICE);
}

/**
 * Validates the human and machine-readable names when adding or editing a menu.
 */
function usermenus_edit_menu_validate($form, &$form_state) {
  $item = $form_state['values'];
  if (preg_match('/[^a-z0-9-]/', $item['menu_name'])) {
    form_set_error('menu_name', t('The menu name may only consist of lowercase letters, numbers, and hyphens.'));
  }
  if (drupal_strlen($item['menu_name']) > MENU_MAX_MENU_NAME_LENGTH_UI) {
    form_set_error('menu_name', format_plural(MENU_MAX_MENU_NAME_LENGTH_UI, "The menu name can't be longer than 1 character.", "The menu name can't be longer than @count characters."));
  }
  if ($form['#insert']) {
    // We will add 'usermenu-' to the menu name to help avoid name-space conflicts.
    $item['menu_name'] = 'usermenu-'. $item['menu_name'];
    if (db_result(db_query("SELECT menu_name FROM {menu_users} WHERE menu_name = '%s'", $item['menu_name'])) ||
      db_result(db_query_range("SELECT menu_name FROM {menu_links} WHERE menu_name = '%s'", $item['menu_name'], 0, 1))) {
      form_set_error('menu_name', t('The menu already exists.'));
    }
  }
}

/**
 * Submit function for adding or editing a custom menu.
 */
function usermenus_edit_menu_submit($form, &$form_state) {
  $menu = $form_state['values'];
  $path = 'admin/build/usermenus-customize/';
  if ($form['#insert']) {
    // Add 'usermenu-' to the menu name to help avoid name-space conflicts.
    $menu['menu_name'] = 'usermenu-'. $menu['menu_name'];
    $link['link_title'] = $menu['title'];
    $link['link_path'] = $path . $menu['menu_name'];
    $link['router_path'] = $path .'%';
    $link['module'] = 'usermenus';
    $link['plid'] = db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s' AND module = '%s'", 'admin/build/usermenus', 'system'));
    menu_link_save($link);
    db_query("INSERT INTO {menu_users} (menu_name, title, description) VALUES ('%s', '%s', '%s')", $menu['menu_name'], $menu['title'], $menu['description']);
  }
  else {
    db_query("UPDATE {menu_users} SET title = '%s', description = '%s' WHERE menu_name = '%s'", $menu['title'], $menu['description'], $menu['menu_name']);
    $result = db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s'", $path . $menu['menu_name']);
    while ($m = db_fetch_array($result)) {
      $link = menu_link_load($m['mlid']);
      $link['link_title'] = $menu['title'];
      menu_link_save($link);
    }
  }
  $form_state['redirect'] = $path . $menu['menu_name'];
}

/**
 * Menu callback; Check access and present a confirm form for deleting a menu link.
 */
function usermenus_item_delete_page($item) {
  // Links defined via hook_menu may not be deleted. Updated items are an
  // exception, as they can be broken.
  if ($item['module'] == 'system' && !$item['updated']) {
    drupal_access_denied();
    return;
  }
  return drupal_get_form('usermenus_item_delete_form', $item);
}

/**
 * Build a confirm form for deletion of a single menu link.
 */
function usermenus_item_delete_form(&$form_state, $item) {
  global $user;
  $form['#item'] = $item;
  if (($item['menu_name']=='usermenu-'. $user->name) && user_access('administer own usermenus')) {
    return confirm_form($form, t('Are you sure you want to delete the custom menu item %item?', array('%item' => $item['link_title'])), 'usermenus/mine/');
  }
  else {
    return confirm_form($form, t('Are you sure you want to delete the custom menu item %item?', array('%item' => $item['link_title'])), 'admin/build/usermenus-customize/'. $item['menu_name']);
  }
}

/**
 * Process menu delete form submissions.
 */
function usermenus_item_delete_form_submit($form, &$form_state) {
  global $user;
  $item = $form['#item'];
  menu_link_delete($item['mlid']);
  $t_args = array('%title' => $item['link_title']);
  drupal_set_message(t('The user menu item %title has been deleted.', $t_args));
  watchdog('menu', 'Deleted user menu item %title.', $t_args, WATCHDOG_NOTICE);
  if (($item['menu_name']=='usermenu-'. $user->name) && user_access('administer own usermenus')) {
    $form_state['redirect'] = 'usermenus/mine/';
  }
  else {
    $form_state['redirect'] = 'admin/build/usermenus-customize/'. $item['menu_name'];
  }
}

